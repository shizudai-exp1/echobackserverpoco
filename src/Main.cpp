//============================================================================
// Name        : Main.cpp
// Author      : Yasuhiro Noguchi
// Version     : 0.0.1
// Copyright   : GPLv2
// Description : EchoBackServer
//============================================================================
#include "EchoBackServer.h"

int main(int argc, char** argv) {
	EchoBackServer ebs;
	ebs.start(8000);
	return 0;
}
