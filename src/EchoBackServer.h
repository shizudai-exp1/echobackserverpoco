/*
 * EchoBackSerrver.h
 *
 *  Created on: 2016/03/08
 *      Author: yasuh
 */

#ifndef ECHOBACKSERVER_H_
#define ECHOBACKSERVER_H_

#include <cstddef>

class EchoBackServer {

public:
	EchoBackServer();
	virtual ~EchoBackServer();

	void start(int port);
};

#endif /* ECHOBACKSERVER_H_ */
