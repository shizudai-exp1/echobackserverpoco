/*
 * EchoBackServer.cpp
 *
 *  Created on: 2016/03/08
 *      Author: yasuh
 */
#include "EchoBackServer.h"
#include "Poco/Net/ServerSocket.h"
#include "Poco/FIFOBuffer.h"
#include <iostream>

using namespace Poco;
using namespace Poco::Net;
using namespace std;

EchoBackServer::EchoBackServer() {
}

EchoBackServer::~EchoBackServer() {
}

void EchoBackServer::start(int port) {
	ServerSocket socket;
	socket.bind(port);
	socket.listen(64);
	StreamSocket stream = socket.acceptConnection();

	// show remote host
	SocketAddress address = stream.peerAddress();
	cout << "accept: " << address.host().toString() << ":" << address.port() << endl;

	try {
		char recv_message[256] = "";
		uint32_t size = stream.receiveBytes(recv_message, 256);
		while (size > 0) {

			// console out
			cout.write(recv_message, size);
			cout.flush();

			// echo back to the client
			stream.sendBytes(recv_message, size);
			size = stream.receiveBytes(recv_message, 256);
		}
	} catch (const Exception & e) {
		cerr << e.what() << endl;
	}
}
